# ------------------------------------------------------------------------------
# Pull base image
FROM ubuntu:jammy

LABEL maintainer="Avet <babuliyan@gmail.com>"
LABEL build_date="07-05-2024"


# ------------------------------------------------------------------------------
# Set environment variables
ENV DEBIAN_FRONTEND noninteractive
ENV LANG ru_RU.UTF-8
ENV LC_ALL ru_RU.UTF-8
ENV DISPLAY=:1

# ------------------------------------------------------------------------------
# Install tigervnc,openbox and clean up
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      ca-certificates \
      curl \
      dbus-x11 \
      fbpanel \
      hsetroot \
      less \
      locales \
      nano \
      openbox \
      sudo \
      tigervnc-common \
      tigervnc-standalone-server \
      tigervnc-tools \
      tzdata \
      wget \
      x11-utils \
      x11-xserver-utils \
      xfonts-base \
      xterm && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

# ------------------------------------------------------------------------------
# Configure locale
RUN echo "LC_ALL=ru_RU.UTF-8" >> /etc/environment && \
    echo "LANG=ru_RU.UTF-8" > /etc/locale.conf && \
    echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen && \
    locale-gen

# ------------------------------------------------------------------------------
# Configure XTerm
RUN sed -e 's/saveLines: 1024/saveLines: 8192/' -i /etc/X11/app-defaults/XTerm

# ------------------------------------------------------------------------------
# Configure openbox
RUN mkdir -p /usr/share/ubuntu-desktop/openbox && \
    cat /etc/xdg/openbox/rc.xml \
      | sed -e 's@<number>4</number>@<number>8</number>@' \
      > /usr/share/ubuntu-desktop/openbox/rc.xml

# ------------------------------------------------------------------------------
# Install scripts and configuration files
COPY app/app.sh app/imagestart.sh app/tiger.sh /app/
COPY bin/set_wallpaper.sh /usr/bin/
COPY conf/xstartup /usr/share/ubuntu-desktop/vnc/
COPY conf/autostart conf/menu.xml /usr/share/ubuntu-desktop/openbox/
COPY conf/fbpaneldefault /usr/share/ubuntu-desktop/fbpanel/default
COPY conf/sudo /usr/share/ubuntu-desktop/sudo
COPY conf/bash.colors conf/color_prompt.sh conf/lang.sh /opt/bash/
COPY scripts/*.sh /app/scripts/

RUN chmod -R 777 /app /usr/bin/ /usr/share/ubuntu-desktop/vnc/ /usr/share/ubuntu-desktop/openbox/ /usr/share/ubuntu-desktop/fbpanel/default /opt/bash/

RUN apt-get update \
    && apt-get -y --no-install-recommends install gstreamer1.0-plugins-bad geoclue-2.0 openssh-server

## Configure ssh 
RUN mkdir -p /var/run/sshd &&  \
    echo 'root:root' | chpasswd && \
    sed -i 's/#*PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config && \
    sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config && \
    sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd

# COPY ./ubuntu-desktop.pub /root/ubuntu-desktop.pub

RUN mkdir /root/.ssh && \
    chmod 700 /root/.ssh && \
    touch /root/.ssh/authorized_keys && \
    chmod 600 /root/.ssh/authorized_keys 
    # && cat /root/ubuntu-desktop.pub >> /root/.ssh/authorized_keys


COPY ./DIST /opt/onec/DIST
COPY ./Core_Fonts /usr/share/fonts/Core_Fonts
# COPY ./CRM /root/CRM

RUN chmod -R 777 /opt/onec

RUN for file in /opt/onec/DIST/*.run; do \
        if [ -f "$file" ]; then \
            "$file" --mode unattended --enable-components server,ws,server_admin,client_full,client_thin,client_thin_fib,config_storage_server,liberica_jre; \
        fi; \
    done

RUN rm -rf /opt/onec/DIST/*

COPY ./nethasp.ini /opt/1cv8/conf/nethasp.ini

# ------------------------------------------------------------------------------
# Expose ports
EXPOSE 5901 22

# ------------------------------------------------------------------------------
# Define default command
CMD ["/app/app.sh"]
