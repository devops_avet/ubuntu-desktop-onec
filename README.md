## Данный образ создан на базе [Fullaxx/ubuntu-desktop](https://github.com/Fullaxx/ubuntu-desktop). Добавлено все необходимое для запуска тестирования Vanessa Automation.

### Последовательность действий

#### I. ВНИМАНИЕ!!!.

1. Для получения лицензии от сервера `HASP`, указываем ip адрес `NH_SERVER_ADDR` в `docker-compose.yml`.

2. Для установки своей версии платформы 1с необходимо добавить соответствующие файл в папку DIST (нужен именно установочный файл 1с с расширением *.run), и запустить свою сборку по команде `docker build`.

#### II. Для локального запуска используем docker-compose.yml

1. Клонируем репозиторий на хост.
```bash
git clone https://gitlab.com/devops_avet/ubuntu-desktop-onec.git
```

2. Переходим в скачанный репозиторий на хосте.
```bash
cd ubuntu-desktop-onec
```

3. Запускаем.
```bash
docker compose up -d
```
4. Доступ к рабочему столу получаем по ip-адресу хоста и порту '8081'.